from rest_framework import serializers
from .models import ToDoItem


class ToDoItemSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='todoitem-detail')

    class Meta:
        model = ToDoItem
        fields = ('id', 'url', 'title', 'text', 'created')
