from .models import ToDoItem
from .serializers import ToDoItemSerializer
from rest_framework import viewsets


class ToDoViewSet(viewsets.ModelViewSet):
    queryset = ToDoItem.objects.all()
    serializer_class = ToDoItemSerializer
